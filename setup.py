NAME = 'fsnotetaking'
VERSION = '0.0.1'
DESCRIPTION = 'Mark path entries with notes'
LONG_DESCRIPTION = """
Mark path entries with notes

Name pending
"""


from setuptools import setup, find_packages

setup(
    name=NAME,
    version=VERSION,
    packages=find_packages(),
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    install_requires=[
        'click',
    ],
    tests_require=[
        'pytest',
        'pytest-dependency',
    ],
    entry_points='''
        [console_scripts]
        fsnotetaking=fsnotetaking.click_subcommands:main
    ''',
)
