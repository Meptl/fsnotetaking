def jira_expands(node):
    """
    Outputs Confluence XHTML.

    Note that Confluence's wiki markup does not support nested macros with the
    same name.

    Next snippet probably does not belong here. Because my Confluence server
    does not have a source editor, I'm going to leave some javascript that can
    overwrite a page's contents.

    ```
newBodyText = `NEW BODY CONTENT`
jQuery.ajax({
    url: contextPath + "/rest/api/content/" + AJS.params.pageId + "?expand=body.storage,version,ancestors",
    success: function(response) {
        response.body.storage.value = newBodyText;
        response.version.number += 1;
        jQuery.ajax({
            contentType: 'application/json',
            type: 'PUT',
            url: contextPath + "/rest/api/content/" + AJS.params.pageId,
            data: JSON.stringify(response),
            success: function(response) {
                console.log(response);
                console.log("Success!");
            },
            error: function(response) {
                console.log(response);
                console.log("Error!");
            }
        });
    }
});
    ```
    """
    ret = '<ac:macro ac:name="expand">'
    ret += f'<ac:parameter ac:name="title">{node.name}</ac:parameter>'
    ret += '<ac:rich-text-body>'
    if node.note:
        ret += node.note
    for child in node.children.values():
        ret += jira_expands(child)
    ret += '</ac:rich-text-body></ac:macro>'
    return ret
