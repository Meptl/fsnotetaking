import click
from fsnotetaking.backend.sqlite import SqliteBackend


@click.command()
@click.argument('path', required=False)
@click.option('--all', is_flag=True, help='Delete all table entries, ignore PATH argument.')
def delete(path, all):
    '''Delete a note entry for a path.'''
    if not path and not all:
        ctx = click.get_current_context()
        click.echo(ctx.get_help())
        click.echo("\n")
        click.echo('Error: Missing argument "PATH" or option "--all".')
        raise click.Abort()

    backend = SqliteBackend()
    if all:
        backend.delete_notes()
    else:
        # TODO: This doesn't work because we have path conversions to DB.
        note = backend.get_note(path)
        if not note:
            click.echo(f'No note for path: {path}')
        else:
            backend.delete_note(path)
