import click

from .add import add
from .delete import delete
from .export import export


# Shortest unique prefix aliasing.
class AliasedGroup(click.Group):
    def get_command(self, ctx, cmd_name):
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx)
                   if x.startswith(cmd_name)]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail('Too many matches: %s' % ', '.join(sorted(matches)))


@click.group(cls=AliasedGroup)
def cli():
    """Tool to mark file system paths with lengthy notes."""
    pass


cli.add_command(add)
cli.add_command(delete)
cli.add_command(export)


def main():
    cli()
